import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Main from '@/pages/Main'
import Dashboard from '@/pages/Dashboard'
import Member from '@/pages/Member'
import From from '@/pages/From'
import Customer from '@/pages/Customer'
import Table from '@/pages/Table'
import JYTable from '@/pages/JYTable'
import JYField from '@/pages/JYField'
let routes = [{
  path: '/',
  component: Main,
  hidden: true,
  children: [{
    path: '/',
    component: Dashboard,
    name: '首页'
  }]
}]

routes.push({
  path: '/member',
  name: '基础信息维护',
  component: Main,
  iconCls: 'fa fa-user-circle-o',
  children: [{
    path: '/from/test',
    component: From,
    name: '表单测试'
  },{
    path: '/list/cus',
    component: Customer,
    name: '客户管理'
  },{
    path: '/list/tab',
    component: Table,
    name: '客户表信息管理'
},{
    path: '/list/jyTab',
    component: JYTable,
    name: '表字典信息'
  },{
    path: '/list/jyField',
    component: JYField,
    name: '字段字典信息'
  }
  ]
})

const router = new Router({
  routes: routes
})

export default router
